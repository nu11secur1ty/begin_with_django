# Creating django first project. 

**1-First we have to install virtualenv :**
```  
   apt install python3-pip
   pip3 install --upgrade pip
   pip3 install virtualenv 
```
**2-Create a new environment named 'nu11secur1ty'** 
```  
  virtualenv --python=python3 nu11secur1ty 
```
**3-Go to the nu11secur1ty directory:**
```  
   cd _your_path_ nu11secur1ty/ 
```
**4- Activate the virtual environment:**
```   
   source bin/activate 
```
**5-Install django with the pip command:**
```  
  pip3 install django==1.9
  pip3 install Django==2.1.3 # in my case
```
**6-Create a new project (I called it djangoapp)** 
```
  django-admin startproject djangoapp
```  
- It will create a new directory djangoapp that contains the django files: 

```  
   ls djangoapp 
```
**7-Go to the project directory:**
``` 
  cd _your_path_ djangoapp/ 
```
**8-Run the 'manage.py' file :** 
```
  python manage.py runserver 
  python3 manage.py runserver
```
**9-Check in the browser :**
```  
  (Default port is 8000) http://localhost:8000/ 
  (Quit by clicking on Ctrl+c) 
```
**10-Next, we will configure the Django admin. Django will automatically generate the database for a superuser. Before we create the superuser, run the command below:**
``` 
      python manage.py migrate 
      python3 manage.py migrate
```   
 - migrate: make adds the models (adding fields, deleting etc.) into the database scheme, the default database is sqlite3. 

11-Now create the admin/superuser: 
```   
   python manage.py createsuperuser
   python3 manage.py createsuperuser
```   
 - Enter a username, Email and a password for the user. 

**12-Run the 'manage.py' file :**
```  
   python manage.py runserver
   python3 manage.py runserver
```
**13-Visit the django admin page :**
```
    http://localhost:8000/admin 
```
13-Login with username admin and your password There you go, your first django project is created.

![](https://github.com/nu11secur1ty/begin_with_django/blob/master/screen/Screenshot%20from%202018-11-24%2001-02-50.png)



